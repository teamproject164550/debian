#use wml::debian::translation-check translation="928673d8ddad60984a8ca39812f17cf93166e398"
<define-tag description>обновление безопасности</define-tag>
<define-tag moreinfo>
<p>Несколько уязвимостей были обнаружены в Heimdal, реализации 
Kerberos 5, которая должна быть совместима с MIT Kerberos.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3671">CVE-2021-3671</a>

    <p>Джозеф Саттон обнаружил, что Heimdal KDC не проверяет 
    наличие имени сервера в TGS-REQ перед разыменованием, 
    что может привести к отказу в обслуживании.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-44758">CVE-2021-44758</a>

    <p>Было обнаружено, что Heimdal склонен к разыменованию NULL в 
    акцепторах, где исходный токен SPNEGO не имеет приемлемых 
    механизмов, что может привести к отказу в обслуживании серверного 
    приложения, использующего SPNEGO.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3437">CVE-2022-3437</a>

    <p>При использовании 1DES, 3DES или RC4 (arcfour) было обнаружено
    несколько ошибок переполнения буфера и непостоянных утечек времени </p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-41916">CVE-2022-41916</a>

    <p>Был обнаружен доступ к памяти за пределами дозволенного когда Хеймдал 
    нормализовал Unicode, что могло привести к отказу в обслуживании.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42898">CVE-2022-42898</a>

    <p>Обнаружено, что целочисленное переполнение при анализе PAC может привести 
    к отказу в обслуживании для Heimdal KDC или, возможно, серверов Heimdal.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-44640">CVE-2022-44640</a>

    <p>Обнаружено, что ASN.1 компилятор Хеймдала генерирует код 
    , который позволяет специально созданным кодировкам DER вызывать недопустимую 
    свободу в декодированной структуре в случае ошибки декодирования, что может привести к 
    удаленному выполнению кода в Heimdal KDC.</p></li>

</ul>

<p>Для стабильной дистрибуции (bullseye),  эти проблемы исправлены в 
версии 7.7.0+dfsg-2+deb11u2.</p>

<p>Мы рекомендуем вам обновить пакеты heimdal.</p>

<p>Для получения подробной информации о статусе безопасности heimdal обратитесь к 
странице отслеживания безопасности по адресу:
<a href="https://security-tracker.debian.org/tracker/heimdal">https://security-tracker.debian.org/tracker/heimdal</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5287.data"
